package org.sensuapp.mobile.dashboard;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.sensuapp.mobile.dashboard.db.AlertDatabaseHelper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class EventsManager
{
  private static EventsManager instance;
  private Random random;
  private AlertDatabaseHelper dbHelper = new AlertDatabaseHelper(Util.getContext());

  private EventsManager()
  {
    random = new Random();
  }

  public static EventsManager getInstance()
  {
    if (instance == null)
      instance = new EventsManager();
    return instance;
  }

  public boolean acceptJsonString(String jsonString)
  {
    try
    {
      return acceptJson(new JSONArray(jsonString));
    }
    catch (JSONException e)
    {
      Log.e("acceptJsonString", e.getLocalizedMessage(), e);
      // TODO: throw Notification for bad JSON?
    }
    return false;
  }

  public boolean acceptJson(JSONArray json)
  {
    boolean bAlert = false;  // return true if dirty
    int touched = random.nextInt();
    try
    {
      final int len = json.length();
      for (int x = 0; x < len; x++)
      {
        Event event = new Event();
        JSONObject object = json.getJSONObject(x);
        event.setStatus(object.getInt("status"));
        event.setOutput(object.getString("output"));
        event.setFlapping(object.getBoolean("flapping"));
        event.setOccurrences(object.getLong("occurrences"));
        event.setClient(object.getString("client"));
        event.setCheck(object.getString("check"));
        event.setIssued(new Date(object.getLong("issued") * 1000));
        List<String> handlerList = new ArrayList<String>();
        JSONArray handlers = object.getJSONArray("handlers");
        final int length = handlers.length();
        for (int y = 0; y < length; y++)
          handlerList.add(handlers.getString(y));
        event.setHandlers(handlerList);

        Log.d("EventManager", "Loaded " + event.getId());

        // save to db
        dbHelper.insert(event, touched);

      }
      return bAlert;
    }
    catch (JSONException e)
    {
      e.printStackTrace();
      // TODO: throw Notification for bad JSON?
    }
    return false;
  }

  public synchronized Collection<Event> getEvents()
  {
    //TODO
      return new ArrayList<Event>();
  }

  public String getNotificationTitle()
  {
    return "Sensu notification title";
  }

  public String getNotificationSummary()
  {
    return "Sensu summary text\nLots of text\nmore...";
  }
}
