package org.sensuapp.mobile.dashboard;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.PowerManager;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


public class Autostart
    extends BroadcastReceiver
{
  private final static Logger log = LoggerFactory.getLogger(Autostart.class);
  private final static String TAG = "AutoStart";

  @Override
  public void onReceive(Context context, Intent intent)
  {
    Log.d("AutoStart", "onReceive fired.");
    PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
    PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");
    wl.acquire();

    String action = intent.getAction();
    if (action != null && action.equals("android.intent.action.BOOT_COMPLETED"))
    {
      SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
      int freq = Integer.parseInt(preferences.getString("sync_frequency", "5"));
      log.debug(TAG, "Frequency set to {}", freq);
      startPolling(context, freq);
      return;
    }

    if (intent.hasExtra("stop"))
      stopPolling(context);

    if (intent.hasExtra("frequency"))
    {
      int foo = intent.getIntExtra("frequency", 999);
      int freq = intent.getIntExtra("frequency", 5);
      log.debug(TAG, "Frequency set to {}", freq);
      startPolling(context, freq);
    }

    if (intent.hasExtra("refresh"))
    {
      Log.d(TAG, "triggered by refresh");
      fetchJson(context);
      Intent response = new Intent("android.intent.action.MAIN");
      context.sendBroadcast(response);
    }

    if (intent.hasExtra("android.intent.extra.ALARM_COUNT"))
    {
      Log.d(TAG, "triggered by alarm");
      fetchJson(context);
      Intent response = new Intent("android.intent.action.MAIN");
      response.putExtra("poll", true);
    }

    wl.release();
  }

  public void startPolling(Context context, int minutes)
  {
    stopPolling(context);
    if (minutes < 0)
      return;
    AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    Intent i = new Intent(context, Autostart.class);
    PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
    long delay = 1000 * 60 * minutes;
    am.setRepeating(AlarmManager.RTC_WAKEUP,
                    System.currentTimeMillis() + delay,
                    delay,
                    pi); // Millisec * Second * Minute
    Log.d(TAG, "polling, delay: " + minutes);
  }

  public void stopPolling(Context context)
  {
    Intent intent = new Intent(context, Autostart.class);
    PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
    AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    alarmManager.cancel(sender);
    Log.d(TAG, "stopped polling");
  }

  public synchronized void fetchJson(Context context)
  {
    SharedPreferences preferences = Util.getContext().getSharedPreferences("preferences", Context.MODE_PRIVATE);
    boolean alert = preferences.getBoolean("notifications_new_message", false);
    Log.d(TAG, "Notification on new message?:" + alert);
    if (!isNetworkAvailable(context))
    {
      Log.d(TAG, "No Internet connection!");
      Toast.makeText(context,
                     context.getString(R.string.error_no_internet_connection),
                     Toast.LENGTH_LONG).show();
//      Uri ringToneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
      NotificationManager notificationManager = (NotificationManager) context
          .getSystemService(Context.NOTIFICATION_SERVICE);
      Intent intent = new Intent(context, MainActivity.class);
      PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
      Notification notification = new Notification.Builder(context)
          .setContentTitle(context.getString(R.string.error_no_internet_connection))
          .setContentText(context.getString(R.string.error_no_internet_connection_notification))
          .setStyle(new Notification.BigTextStyle().bigText(context
                                                                .getString(
                                                                    R.string.error_no_internet_connection_notification)))
          .setContentIntent(pendingIntent)
//          .setAutoCancel(true)
          .setSmallIcon(R.drawable.ic_launcher).build();
//      notificationId = 1;

//      notificationManager.notify(notificationId, notification);
      notificationManager.notify(0, notification);
      if (alert)
      {
        Uri ringToneUri = Uri.parse(preferences.getString("notification_ringtone",
                                                          RingtoneManager.getDefaultUri(
                                                              RingtoneManager.TYPE_NOTIFICATION)
                                                              .toString()));
        Ringtone ringtone = RingtoneManager.getRingtone(context, ringToneUri);
        //ringtone.play();
      }

      if (preferences.getBoolean("notification_vibrate", false))
      {
        // Get instance of Vibrator from current Context
        Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(new long[]{0, 100, 100, 100, 100, 100}, -1);
      }
      return;
    }
    else // if (notificationId == 1)
    {
      NotificationManager notificationManager = (NotificationManager) context
          .getSystemService(Context.NOTIFICATION_SERVICE);
      notificationManager.cancelAll();
//      notificationId = 0;
    }

    AsyncTask task = new Fetch().execute(alert);
    int timeout = 10000;
    try
    {
      task.get(timeout, TimeUnit.MILLISECONDS);
    }
    catch (InterruptedException e)
    {
      e.printStackTrace();
    }
    catch (ExecutionException e)
    {
      e.printStackTrace();
    }
    catch (TimeoutException e)
    {
      Toast.makeText(context,
                     context.getString(R.string.error_connection_timeout),
                     Toast.LENGTH_LONG).show();
      task.cancel(true);
    }
  }

  public boolean isNetworkAvailable(Context context)
  {
    ConnectivityManager cm = (ConnectivityManager)
        context.getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo networkInfo = cm.getActiveNetworkInfo();
    // if no network is available networkInfo will be null
    // otherwise check if we are connected
    return networkInfo != null && networkInfo.isConnected();
  }
}