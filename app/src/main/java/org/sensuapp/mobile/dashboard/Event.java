package org.sensuapp.mobile.dashboard;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Event
    implements Comparable,
               Serializable
{
  public final static int HIGH = 2;
  public final static int MEDIUM = 1;
  public final static int UNKNOWN = 0;
  public final static int SORT_DATE_ASCENDING = 0;
  public final static int SORT_DATE_DESCENDING = 1;

  private String output;
  private int status;
  private Date issued;
  private List<String> handlers;
  private boolean flapping;
  private long occurrences;
  private String client;
  private String check;
  private boolean muted;
  private boolean resolved;
  private int sortType;
  private boolean read = false;

  public String getId()
  {
    return client + "-" + check;
  }

  public String getOutput()
  {
    return output;
  }

  public void setOutput(String output)
  {
    this.output = output;
  }

  public int getStatus()
  {
    return status;
  }

  public void setStatus(int status)
  {
    this.status = status;
  }

  public Date getIssued()
  {
    return issued;
  }

  public void setIssued(Date issued)
  {
    this.issued = issued;
  }

  public List<String> getHandlers()
  {
    return handlers;
  }

  public void setHandlers(List<String> handlers)
  {
    this.handlers = handlers;
  }

  public boolean isFlapping()
  {
    return flapping;
  }

  public void setFlapping(boolean flapping)
  {
    this.flapping = flapping;
  }

  public long getOccurrences()
  {
    return occurrences;
  }

  public void setOccurrences(long occurrences)
  {
    this.occurrences = occurrences;
  }

  public String getClient()
  {
    return client;
  }

  public void setClient(String client)
  {
    this.client = client;
  }

  public String getCheck()
  {
    return check;
  }

  public void setCheck(String check)
  {
    this.check = check;
  }

  public boolean isResolved()
  {
    return resolved;
  }

  public void setResolved(boolean value)
  {
    resolved = value;
  }

  public boolean isMuted()
  {
    return muted;
  }

  public void setMuted(boolean value)
  {
    muted = value;
  }

  public int getSortType()
  {
    return sortType;
  }

  public void setSortType(int type)
  {
    sortType = type;
  }

  public void markRead()
  {
    read = true;
  }

  public boolean isRead()
  {
    return read;
  }

  public boolean hasChanged(Event oldEvent)
  {
    return oldEvent.occurrences < occurrences || !oldEvent.issued.equals(issued)
           || oldEvent.resolved != resolved || oldEvent.flapping != flapping;
  }

  /**
   * Compares this object to the specified object to determine their relative
   * order.
   *
   * @param another the object to compare to this instance.
   * @return a negative integer if this instance is less than {@code another};
   * a positive integer if this instance is greater than
   * {@code another}; 0 if this instance has the same order as
   * {@code another}.
   * @throws ClassCastException if {@code another} cannot be converted into something
   *                            comparable to {@code this} instance.
   */
  @Override
  public int compareTo(Object another)
  {
    Event event = (Event) another;
    switch (sortType)
    {
      case SORT_DATE_DESCENDING:
        return issued.compareTo(event.getIssued()) * -1;
      case SORT_DATE_ASCENDING:
        return issued.compareTo(event.getIssued());
      default:
        return issued.compareTo(event.getIssued());
    }

  }
}