package org.sensuapp.mobile.dashboard.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.provider.BaseColumns;
import android.util.Log;

import org.sensuapp.mobile.dashboard.Event;

public class AlertDatabaseHelper
  extends SQLiteOpenHelper
{
  public static final String TAG = "AlertProvider";
  public static final String DATABASE_NAME = "Sensu";
  public static final String ALERT_TABLE = "Alerts";
  public static final String CREATE_SQL = "CREATE TABLE " + ALERT_TABLE + " ("
                                          + AlertColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                                          + AlertColumns.ISSUED + " INTEGER DEFAULT 0 NOT NULL, "
                                          + AlertColumns.OUTPUT + " TEXT, "
                                          + AlertColumns.STATUS + " INTEGER DEFAULT 0 NOT NULL, "
                                          + AlertColumns.FLAPPING + " INTEGER DEFAULT 0 NOT NULL, " // Boolean value 0 and 1
                                          + AlertColumns.CLIENT + " TEXT, "
                                          + AlertColumns.CHECK + " TEXT, "
                                          + AlertColumns.HANDLERS + " TEXT, " // comma separated values
                                          + AlertColumns.OCCURRENCES + " INTEGER DEFAULT 0 NOT NULL, "
                                          + AlertColumns.READ + " INTEGER DEFAULT 0 NOT NULL, " // has the alert been viewed?
                                          + AlertColumns.TOUCHED + " INTEGER NOT NULL);";
  public static final String STATUS_INDEX_SQL = "CREATE INDEX " + AlertColumns.STATUS + "_idx ON " + ALERT_TABLE + " ("
                                                + AlertColumns.STATUS + ");";
  public static final String CLIENT_CHECK_INDEX_SQL = "CREATE UNIQUE INDEX CLIENT_CHECK_INDEX_idx ON " + ALERT_TABLE + " ("
                                                      + AlertColumns.CLIENT + ", " + AlertColumns.CHECK + ");";
  public static final int DATABASE_VERSION = 1;
  private SQLiteDatabase database;

  public AlertDatabaseHelper(Context context)
  {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
    database = getWritableDatabase();
  }

  @Override
  public void onCreate(SQLiteDatabase database)
  {
    Log.d(TAG, "Create SQL: " + CREATE_SQL);
    database.execSQL(CREATE_SQL);
    Log.d(TAG, "Create SQL: " + STATUS_INDEX_SQL);
    database.execSQL(STATUS_INDEX_SQL);
    Log.d(TAG, "Create SQL: " + CLIENT_CHECK_INDEX_SQL);
    database.execSQL(CLIENT_CHECK_INDEX_SQL);
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
  {
    switch (oldVersion)
    {
      case 2:
        // TODO: Alter table for from 1 to 2
        break;
      default:
        break;
    }
  }

  public void insert(Event event, int touched)
  {
    {
      StringBuilder sql = new StringBuilder(100);
      StringBuilder handlers = new StringBuilder();
      sql.append("insert into ").append(ALERT_TABLE).append(" (").append(AlertColumns.STATUS).append(",")
         .append(AlertColumns.OUTPUT)
         .append(",").append(AlertColumns.ISSUED).append(",").append(AlertColumns.FLAPPING).append(",")
         .append(AlertColumns.OCCURRENCES)
         .append(",").append(AlertColumns.CLIENT).append(",").append(AlertColumns.CHECK).append(",").append(AlertColumns.HANDLERS)
         .append(",").append(AlertColumns.TOUCHED).append(") values (?,?,?,?,?,?,?,?,?)");
      Log.d(TAG, sql.toString());
      SQLiteStatement statement = database.compileStatement(sql.toString());
      statement.bindLong(1, event.getStatus());
      statement.bindString(2, event.getOutput());
      statement.bindLong(3, event.getIssued().getTime());
      statement.bindLong(4, event.isFlapping() ? 1 : 0);
      statement.bindLong(5, event.getOccurrences());
      statement.bindString(6, event.getClient());
      statement.bindString(7, event.getCheck());
      for (String handler : event.getHandlers())
      {
        if (handlers.length() != 0)
          handlers.append('|');
        handlers.append(handler);
      }
      statement.bindString(8, handlers.toString());
      statement.bindLong(9, touched);

      try
      {
        database.beginTransaction();
        statement.execute();
        database.setTransactionSuccessful();
      }
      catch (Exception e)
      {
        // TODO - don't do it this way.  Batch insert with two arrays of updates and inserts, then delete
        update(event, touched);
      }
      finally
      {
        database.endTransaction();
        statement.clearBindings();
      }
    }
  }

  public void update(Event event, int touched)
  {
    {
      StringBuilder sql = new StringBuilder(100);
      StringBuilder handlers = new StringBuilder();
      sql.append("update ").append(ALERT_TABLE).append(" set ").append(AlertColumns.STATUS).append("=?,")
         .append(AlertColumns.OUTPUT)
         .append("=?,").append(AlertColumns.ISSUED).append("=?,").append(AlertColumns.FLAPPING).append("=?,")
         .append(AlertColumns.OCCURRENCES)
         .append("=?,").append(AlertColumns.HANDLERS)
         .append("=?,").append(AlertColumns.TOUCHED).append("=? where ").append(AlertColumns.CLIENT).append("=? and ")
         .append(AlertColumns.CHECK)
         .append("=?");
      Log.d(TAG, sql.toString());
      SQLiteStatement statement = database.compileStatement(sql.toString());
      statement.bindLong(1, event.getStatus());
      statement.bindString(2, event.getOutput());
      statement.bindLong(3, event.getIssued().getTime());
      statement.bindLong(4, event.isFlapping() ? 1 : 0);
      statement.bindLong(5, event.getOccurrences());
      for (String handler : event.getHandlers())
      {
        if (handlers.length() != 0)
          handlers.append('|');
        handlers.append(handler);
      }
      statement.bindString(6, handlers.toString());
      statement.bindLong(7, touched);
      statement.bindString(8, event.getClient());
      statement.bindString(9, event.getCheck());

      try
      {
        database.beginTransaction();
        statement.execute();
        database.setTransactionSuccessful();
      }
      finally
      {
        database.endTransaction();
        statement.clearBindings();
      }
    }
  }
}