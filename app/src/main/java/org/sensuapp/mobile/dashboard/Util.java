package org.sensuapp.mobile.dashboard;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by kduling on 4/4/14.
 */
public class Util
    extends Application
{
  private static Context context;

  public static Context getContext()
  {
    return Util.context;
  }

  public void onCreate()
  {
    super.onCreate();
    Util.context = getApplicationContext();
    SharedPreferences preferences = getSharedPreferences("preferences", Context.MODE_PRIVATE);
//    Intent intent = new Intent(this, PollSensu.class);
//    intent.putExtra("frequency", Integer.parseInt(preferences.getString("sync_frequency", "5")));
//    intent.putExtra("refresh", true);
//    sendBroadcast(intent);

  }
}
