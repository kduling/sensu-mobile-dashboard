package org.sensuapp.mobile.dashboard.db;

import android.provider.BaseColumns;

public interface AlertColumns
    extends BaseColumns
{
  String STATUS = "status";
  String OUTPUT = "output";
  String ISSUED = "issued";
  String FLAPPING = "flapping";
  String OCCURRENCES = "occurrences";
  String CLIENT = "client";
  String CHECK = "check_type";
  String HANDLERS = "handlers";
  String TOUCHED = "touched";
  String READ = "read";
}
