package org.sensuapp.mobile.dashboard;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Vibrator;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;

public class Fetch
    extends AsyncTask<Boolean, Integer, Integer>
{
  private final static String TAG = "Fetch";
  public final static int CONNECTION_TIMEOUT = 10;
  public final static int INVALID_URI = 11;
  public final static int UNKNOWN_HOST = 12;
  private final static Logger log = LoggerFactory.getLogger(Autostart.class);

  // Do the long-running work in here
  protected Integer doInBackground(Boolean... alerts)
  {
    Log.d(TAG, "notification: " + alerts[0]);
    boolean alert = alerts[0];
    Context context = Util.getContext();

    SharedPreferences preferences = Util.getContext().getSharedPreferences("preferences", Context.MODE_PRIVATE);
    UsernamePasswordCredentials upc = new UsernamePasswordCredentials(preferences
                                                                          .getString("username",
                                                                                     "demo"),
                                                                      preferences
                                                                          .getString("password",
                                                                                     "demo")
    );
    boolean debug = preferences.getBoolean("debug", false);
    String host = preferences.getString("hostname", "");
    if (host.isEmpty())
      return INVALID_URI;
    int port = Integer.parseInt(preferences.getString("port", "4567"));
    String protocol = "http";
    if (debug)
      protocol = "https";

    try
    {
      URI uri = new URI(protocol + "://" + host + ":" + port + "/events");
      if (debug)
      {
        protocol = "http";
        host = "www.googledrive.com";
        port = 80;
        uri = new URI(protocol + "://" + host + ":" + port + "/host/0B9Lha7Y9_doHTFR1MEtkckI4OTA/ev.txt");
      }
      Log.d(TAG, uri.toASCIIString());
      HttpParams params = new BasicHttpParams();
      HttpConnectionParams.setConnectionTimeout(params, 4000);
      HttpConnectionParams.setSoTimeout(params, 6000);
      HttpClient client = new DefaultHttpClient(params);
      AuthScope as = new AuthScope(host, port);
      ((AbstractHttpClient) client).getCredentialsProvider()
          .setCredentials(as, upc);
      BasicHttpContext localContext = new BasicHttpContext();
      BasicScheme basicAuth = new BasicScheme();
      localContext.setAttribute("preemptive-auth", basicAuth);
      HttpHost targetHost = new HttpHost(host, port, protocol);
      HttpGet httpget = new HttpGet();
      httpget.setURI(uri);
      httpget.setHeader("Content-Type", "application/json");
//          HttpResponse response = client.execute(targetHost, httpget,
//                                                 localContext);
      HttpResponse response = client.execute(targetHost, httpget);
      HttpEntity entity = response.getEntity();
      String content = EntityUtils.toString(entity);
      EventsManager eventsManager = EventsManager.getInstance();
      if (eventsManager.acceptJsonString(content) && alert)
      {
        Uri ringToneUri = Uri.parse(preferences.getString("notification_ringtone",
                                                          RingtoneManager.getDefaultUri(
                                                              RingtoneManager.TYPE_NOTIFICATION)
                                                              .toString()));
        Ringtone ringtone = RingtoneManager.getRingtone(context, ringToneUri);
        NotificationManager notificationManager = (NotificationManager) context
            .getSystemService(Context.NOTIFICATION_SERVICE);
        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        Notification notification = new Notification.Builder(context)
            .setContentTitle(eventsManager.getNotificationTitle())
//                .setContentText(context.getString(R.string.notification_new_data))
            .setStyle(new Notification.BigTextStyle().bigText(eventsManager.getNotificationSummary()))
            .setContentIntent(pendingIntent)
//          .setAutoCancel(true)
            .setSmallIcon(R.drawable.ic_launcher).build();
//      notificationId = 1;

//      notificationManager.notify(notificationId, notification);
        notificationManager.notify(0, notification);
        //ringtone.play();

        if (preferences.getBoolean("notification_vibrate", false))
        {
          // Get instance of Vibrator from current Context
          Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
          v.vibrate(new long[]{0, 500, 100, 500}, -1);
        }
      }
    }
    catch (URISyntaxException e)
    {
      e.printStackTrace();
      return INVALID_URI;
    }

    catch (UnknownHostException uh)
    {
      uh.printStackTrace();
      return UNKNOWN_HOST;
    }

    catch (ConnectTimeoutException connectTimeoutException)
    {
      connectTimeoutException.printStackTrace();
      return CONNECTION_TIMEOUT;
    }

    catch (Exception e)
    {
      e.printStackTrace();
    }
    return 0;
  }

  @Override
  protected void onPostExecute(Integer result)
  {
    Context context = Util.getContext();
    switch (result)
    {
      case CONNECTION_TIMEOUT:
        Toast.makeText(context, context.getString(R.string.error_connection_timeout), Toast.LENGTH_LONG).show();
        break;
      case INVALID_URI:
        Toast.makeText(context, context.getString(R.string.error_invalid_hostname), Toast.LENGTH_LONG).show();
        break;
      case UNKNOWN_HOST:
        Toast.makeText(context, context.getString(R.string.error_unknown_hostname), Toast.LENGTH_LONG).show();
        break;
    }
  }
}
// todo: alert should give a count of events and show the first three deltas?

